﻿
-- tabla provincias 

SELECT  p.provincia,p.poblacion/p.superficie FROM provincias p;

SELECT SQRT(2);
SELECT 2+3;
-- nombre de provincias por autonomía

SELECT autonomia,provincia FROM provincias;

-- cuantos caracteres tiene cada nombre de provincia
  SELECT p.provincia,char_LENGTH( p.provincia) FROM provincias p; 

  -- listados de autonomías
    SELECT DISTINCT p.autonomia FROM provincias p;
   
-- provincias con el mismo nombre que su comunidad autónoma
  SELECT p.autonomia FROM provincias p WHERE p.autonomia= p.provincia;
  
  -- provincias que contienen el diptongo ue
    SELECT p.provincia FROM provincias p WHERE p.provincia LIKE '%ue%';  

-- provincias que empiezan por a
  SELECT p.provincia FROM provincias p WHERE p.provincia LIKE 'a%'; 

-- que provincias están en autonomías con nombre compuesto
  SELECT p.provincia FROM provincias p WHERE p.autonomia LIKE '% %'; 

-- que provincias tienen nombre compuesto
  SELECT p.provincia FROM provincias p WHERE p.provincia LIKE '% %'; 

-- que provincias tienen nombre simple
  SELECT p.provincia FROM provincias p WHERE p.provincia NOT LIKE '% %'; 

-- muestra las provincias de galicia, indicando si es grande, mediana o pequeña 
 --  en función de si su población supera el umbral de un millón o de medio millón de habitantes
SELECT p.provincia, CASE
  WHEN poblacion>1e6 THEN 'Grande'
  WHEN poblacion>5e5 THEN 'Mediana'
  ELSE 'Pequeña'
  END 
  FROM provincias p WHERE p.autonomia='galicia'; 

-- autonomías terminadas en ana
  SELECT DISTINCT p.autonomia FROM provincias p WHERE p.autonomia LIKE '%ana'; 

-- cuantos caracteres tiene cada nombre de comunidad autónoma? 
 -- Ordena el resultado por el nombre de la autonomía de forma descendente
  SELECT DISTINCT p.autonomia, char_LENGTH( p.autonomia) FROM provincias p ORDER BY p.autonomia DESC; 

-- que autonomías tienen provincias de más de un millón de habitantes? Ordénalas alfabéticamente
  SELECT DISTINCT p.autonomia FROM provincias p WHERE p.poblacion >1e6 ORDER BY p.autonomia ASC; 

-- población del país
  SELECT SUM(p.poblacion) FROM provincias p; 

-- cuantas provincias hay en la tabla
  SELECT COUNT(*) FROM provincias p; 

-- que comunidades autónomas contienen el nombre de una de sus provincias
  SELECT DISTINCT p.autonomia FROM provincias p WHERE LOCATE( p.provincia, p.autonomia)>0; 

-- obtén el listado de autonomías (una linea por autonomia, 
  -- junto al listado de sus provincias en una única celda. Recuerda que tras una coma, 
  -- deberia haber un espacio en blanco
  SELECT autonomia, GROUP_CONCAT( provincia SEPARATOR ',') FROM provincias 
  GROUP BY 1; 

-- Listado del número de provincias por autonomía ordenadas de más a menos provincias
  -- y por autonomía en caso de coincidir 
  SELECT COUNT(*),autonomia FROM provincias GROUP BY autonomia 
    ORDER BY COUNT(*) DESC, autonomia;
  
 -- cuantas provincias con nombre compuesto tiene cada comunidad autónoma
  SELECT autonomia,COUNT(*) FROM provincias 
  WHERE provincia LIKE '% %' GROUP BY autonomia;    

-- autonomías uniprovinciales
  SELECT autonomia FROM provincias GROUP BY autonomia HAVING COUNT(*)=1; 

-- que autonomía tiene 5 provincias
  SELECT autonomia FROM provincias GROUP BY autonomia HAVING COUNT(*)=5; 

-- población de la autonomía más poblada
SELECT MAX(p) FROM (
    SELECT SUM(poblacion)  p FROM provincias GROUP BY autonomia
) c1;

-- que porcentaje del total nacional representa cantabria en población y en superficie
SELECT (
  SELECT poblacion FROM provincias WHERE provincia='cantabria'
)/(
  SELECT SUM(poblacion) FROM provincias
)*100,
(
  SELECT superficie FROM provincias WHERE provincia='cantabria'
)/(
SELECT SUM( superficie) FROM provincias 
)*100;

-- obtener la provincia más poblada de cada comunidad autónoma, indicando la población
-- de ésta. Mostrar autonomía, provincia y población por orden de aparición en la tabla
  
-- mostrar todos los campos y todos los registros de la tabla empleado
  

use emple_depart;


