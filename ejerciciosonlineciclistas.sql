﻿USE ciclistas;

-- distancia total recorrida
  SELECT SUM( kms) FROM etapa; 

-- datos de las etapas que no empiecen en la misma ciudad en que acaba la 
-- etapa anterior
  SELECT * FROM etapa WHERE salida<> llegada;
  
-- obtener las poblaciones que tienen la meta de alguna etapa, 
  -- pero desde las que no se realiza ninguna salida. utiliza not in
  SELECT llegada FROM etapa WHERE llegada NOT IN (
      SELECT salida FROM etapa);
  
-- obtener los datos de las etapas que no comiencen en la misma ciudad
-- en que acaba la etapa anterior
  SELECT * FROM ;
  
-- que ciclistas han ganado etapas llevando un maillot
  SELECT DISTINCT nombre FROM lleva JOIN etapa 
    USING( numetapa, dorsal) 
  JOIN ciclista USING(dorsal);    
  
-- obtener el nombre de los puertos de montaña que tienen una altura 
  -- superior a la altura media de todos los puertos
  SELECT AVG( altura) FROM puerto;
  SELECT nompuerto FROM puerto WHERE altura> 
  (SELECT AVG( altura) FROM puerto); 
  
-- numero de la etapa mas larga
  SELECT MAX( kms) FROM etapa;
  SELECT numetapa FROM etapa WHERE kms=( SELECT MAX( kms) FROM etapa);
  
-- cuantos maillots diferentes ha llevado miguel indurain
SELECT dorsal FROM ciclista WHERE nombre='miguel indurain';
SELECT DISTINCT código FROM lleva WHERE dorsal=
  (SELECT dorsal FROM ciclista WHERE nombre='miguel indurain'); 
SELECT COUNT(*)FROM (SELECT DISTINCT código FROM lleva WHERE dorsal=
  (SELECT dorsal FROM ciclista WHERE nombre='miguel indurain')) c1;  

-- cuanto dinero se ha repartido en premios
  SELECT SUM( premio) FROM maillot JOIN lleva USING(código); 

-- listar las etapas que pasan por algún puerto de montaña y que tienen salida
  -- y llegada en la misma población
  SELECT DISTINCT numetapa FROM puerto;
  SELECT * FROM etapa WHERE salida=llegada AND numetapa IN (
    SELECT DISTINCT numetapa FROM puerto);  
          
