﻿-- base de datos emple depart

-- mostrar todo de la tabla empleado
SELECT * FROM emple;

-- mostrar todos los campos y todos los registros de la tabla departamento
  SELECT * FROM depart;
  
-- mostrar el número, nombre y localización de cada depaeramento
  SELECT * FROM depart; 
  
-- datos de los empleados ordenados por número de departamento descendentemente
  SELECT * FROM emple ORDER BY dept_no DESC;  
  
-- datos de los empleados ordenados por número de departamento descendentemente 
-- y por oficio ascendentemente
  SELECT * FROM emple ORDER BY dept_no DESC, oficio ASC;

-- datos de los empleados ordenados por número de departamento descendentemente y por
-- apellido ascendentemente
  SELECT * FROM emple ORDER BY dept_no DESC, apellido ASC;
  
-- mostrar el apellido y oficio de cada empleado
  SELECT apellido, oficio FROM emple;  
  
-- mostrar localización y número de cada departamento
  SELECT loc, dept_no FROM depart;  
  
-- datos de los empleados ordenados por apellido de forma ascendente
  SELECT * FROM emple ORDER BY apellido;    
  
-- datos de los empleados ordenados por apellido de forma descendente
  SELECT * FROM emple ORDER BY apellido DESC; 
  
-- mostrar los datos de los empleados cuyo oficio sea analista
  SELECT * FROM emple WHERE oficio='analista'; 
  
-- mostrar los datos de los empleados cuyo oficio sea analista y ganen más de 2000
  SELECT * FROM emple WHERE oficio='analista' AND salario>2000;  
  
-- listar el apellido de todos los empleados y ordenarlos por oficio y por nombre
  SELECT apellido FROM emple ORDER BY oficio, apellido;  
  
-- seleccionar de la tabla emple los empleados cuyo apellido no termine por z.
  -- Listar todos los campos de la tabla empleados
  SELECT * FROM emple WHERE apellido NOT LIKE '%z';    
  
-- mostrar el codigo de los empleados cuyo salario sea mayor que 2000
  SELECT emp_no FROM emple WHERE salario>2000;  
  
-- mostrar los codigos y apellidos de los empleados cuyo salario sea menor que 2000
  SELECT emp_no, apellido FROM emple WHERE salario<2000;  
  
-- mostrar los datos de los empleados cuyo salario esté entre 1500 y 2500
  SELECT * FROM emple WHERE salario BETWEEN 1500 AND 2500;    
  
-- seleccionar el apellido y oficio de los empleados del departamento número 20
  SELECT apellido, oficio FROM emple WHERE dept_no=20;     
  
-- mostrar todos los datos de empleados cuyos apellidos comiencen por m o por n 
  -- ordendos por apellido de forma ascendente
  SELECT * FROM emple WHERE apellido LIKE 'm%' OR apellido LIKE 'n%' ORDER BY apellido;  
  
  -- seleccionar los datos de los empleados cuyo oficio sea vendedor.
   -- mostrar los datos ordenados por apellido de forma ascendente
            
SELECT * FROM emple WHERE oficio='vendedor' ORDER BY apellido; 

-- mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea analista
-- ordenar el resultado por apellido y oficio de forma ascendente
  SELECT * FROM emple WHERE dept_no=10 AND oficio='analista' 
  ORDER BY apellido,oficio;

-- realizar un listado de los dictintos meses en que los empleados se han dedo de alta
SELECT DISTINCT MONTH( fecha_alt) FROM emple; 

-- realizar un listado de los distintos años en los que loa empleados se han dado
  -- de alta
  SELECT DISTINCT YEAR( fecha_alt) FROM emple; 

-- listar los dias del men en que los empleados se han dado de alta
  SELECT DISTINCT DAY( fecha_alt) FROM emple; 

-- listar los apellidos de los empleados que tengan un salario mayor que 2000
  -- o que pertenezcan al departamento número 20
  SELECT apellido FROM emple WHERE salario>2000 OR dept_no=20; 

  -- listar los empleados cuyo apellido empiece por a o por m.
-- listar el apellido de los empleados
    SELECT apellido FROM emple WHERE apellido LIKE 'a%' OR apellido LIKE 'm%'; 

-- listar los empleados cuyo apellido empiece por a. listar el apellido de los empleados

/* substr muestras el campo a utilizar y el primer numero se pone para listar
 desde que letra empieza a buscar y el segundo numero indica la cantidad de letras
 a buscar. despues se pone igual y la letra en cuestion a buscar*/
SELECT apellido FROM emple WHERE SUBSTR( apellido, 1,1) = 'a' ;

/* listar las filas cuyo apellido empiece por a y el oficio tenga una e en cuaquier
posición. ordenar la salida por oficio y por salario de forma descendente

 con locate se busca lo que contiene un campo en otro,
el primer digito dentro del segundo*/
  SELECT * FROM emple WHERE apellido LIKE 'a%' AND
  LOCATE('e',oficio) ORDER BY oficio, salario DESC; 

-- indicar el número de empleados que hay
  SELECT COUNT(*) FROM emple; 

-- indicar el número de departamentos que hay
  SELECT COUNT(*) FROM depart; 

-- contar el número de empleados cuyo oficio sea vendedor
  SELECT COUNT(*) FROM emple WHERE oficio='vendedor'; 

-- listar el apellido del empleado y el nombre del departamento al que pertenece.
-- ordena el resultado por apellido
  SELECT apellido, dnombre FROM emple JOIN depart USING(dept_no)
  ORDER BY apellido;  

-- listar el apellido del empleado, el oficio del empleado y el nombre del
-- departamento al que pertenece. ordenar por apellido de forma descendente
  SELECT apellido, oficio, dnombre FROM depart JOIN emple USING(dept_no)
  ORDER BY apellido DESC;
  
 -- mostrar los apellido del empleado que más gana
  SELECT MAX(salario) FROM emple;  
  SELECT apellido FROM emple WHERE salario= (SELECT MAX(salario) FROM emple);  
  
-- indicar el número de empleados más el número de departamentos
  SELECT COUNT(*) FROM emple + s; 

  SELECT (
    SELECT COUNT(*) FROM emple
  )+(
    SELECT COUNT(*) FROM depart
  );

-- listar el número de departamento de aquellos departamentos
  -- que tengan empleados y el número de empleados que tengan.
  SELECT emp_no, depart.dept_no FROM emple JOIN depart 
  USING (dept_no) WHERE oficio='empleado'; 


  SELECT dept_no,COUNT(*) FROM emple GROUP BY 1;

-- 